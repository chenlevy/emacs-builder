FROM ubuntu:20.04

RUN apt update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt -y install build-essential autoconf git gcc-10 libgccjit0 \
    libgccjit-10-dev libwebkit2gtk-4.0-dev libgtk-3-dev libtiff5-dev \
    libgif-dev libjpeg-dev libpng-dev libxpm-dev libncurses-dev \
    texinfo libgnutls28-dev
RUN git clone https://git.savannah.gnu.org/git/emacs.git -b feature/native-comp
ARG NATIVE_FULL_AOT=1
ARG CC=/usr/bin/gcc-10
ARG CXX=/usr/bin/cpp-10
ARG VERSION=28.0.50
ENV VERSION=$VERSION
RUN cd /emacs && ./autogen.sh
# RUN export VERSION=$(sed -ne 's:^#define PACKAGE_VERSION "\([^"]*\)":\1:p' /emacs/src/config.h)
RUN cd /emacs && ./configure --prefix=/opt/emacs/$VERSION \
    --with-xwidgets --with-x --with-x-toolkit=gtk3 \
    --with-native-compilation
# RUN cgcreate -g memory:compilation
# echo 6G > /sys/fs/cgroup/memory/compilation/memory.limit_in_bytes
# sudo execcgexec -g memory:compilation ionice -n6 nice -n16 make -j
# && ulimit -l 5000000 \
RUN cd /emacs \
    && ulimit -m 5000000 \
    && ulimit -v 5000000 \
    && ionice -n6 nice -n16 make
RUN cd /emacs && make install
